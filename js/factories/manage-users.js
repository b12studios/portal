app.factory("ManageUsers", function($resource) {
	return $resource('http://166.70.139.17:4567/manage/users/:id', { id: '@_id' }, {
		update: {
			method: 'PUT'
		}
	});
});
