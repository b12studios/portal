app.factory("Searches", function($resource) {
	return $resource('http://166.70.139.17:4567/searches/:id', {id: '@_id'}, {
		update: {
			method: 'PUT'
		}
	});
});
