app.controller("UsersController", function($scope, $state, $stateParams, ManageUsers, popupService, $filter) {

	$scope.usersByPage = 10;
	$scope.roles = [
		{	value: "admin",	text: "Admin"	},
		{	value: "manager",	text: "Manager"	},
		{	value: "investigator", text: "Investigator"	},
		{	value: "triage", text: "Triage"	}
	];

	var promise = null;
  $scope.isLoading = false;
  $scope.getPage = function() {
		$scope.users = ManageUsers.query();
	}

  $scope.callServer = function getData(tableState) {
      //here you could create a query string from tableState
      //fake ajax call
      $scope.isLoading = true;

      $timeout(function () {
          $scope.getPage();
          $scope.isLoading = false;
      }, 2000);
  };

	$scope.getPage();

	var newUser = new ManageUsers();
	$scope.form = {};

	$scope.submitUser = function() {
		newUser.userName = $scope.form.userName;
		newUser.password = $scope.form.password;
		newUser.userRole = $scope.form.userRole;
		newUser.userInfo = {
			firstName: $scope.form.userInfo.firstName,
			lastName: $scope.form.userInfo.lastName,
			email: $scope.form.userInfo.email
		};
		newUser.$save(function(a, b, c, d) {
			$scope.users.push(a);
			console.log(b);
			console.log(c);
			console.log(d);
		});
		$scope.form = {};
	}

	$scope.editUser = function(data, id) {
		var temp = {userRole: data.userRole,
			userInfo: {
				firstName: data.firstName,
				lastName: data.lastName,
				email: data.email
			}
		};
		ManageUsers.update({id: id}, temp);
	};

	$scope.removeUser = function(a, b) {
		if (popupService.showPopup("Are you sure?")) {
			ManageUsers.delete({id: a.id.$oid});
			$scope.users.splice(b, 1);
		}
	};

});
