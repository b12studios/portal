app.controller("ManageClientsController", function($scope, $state, Clients, editableOptions, editableThemes) {
	editableThemes.bs3.inputClass = 'input-sm';
	editableThemes.bs3.buttonsClass = 'btn-sm';
	editableOptions.theme = 'bs3';

	$scope.clients = Clients.query();

	$scope.checkName = function(a, b) {
		console.log(a);
		console.log(b);
	}

	$scope.saveClient = function(data, id) {
		console.log(data);
		console.log(id);
	}

	$scope.removeClient = function(index) {
		console.log(index);
	}
})
