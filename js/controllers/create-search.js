app.controller("CreateSearchController", function($scope, $state, Searches) {
	$scope.form = {};

	var newSearch = new Searches();

	$scope.searchSubmit = function() {
		newSearch.module = $scope.form.module;
		newSearch.boards = $scope.form.boards;
		newSearch.keywords = $scope.form.keywords;
		newSearch.frequency = $scope.form.frequency;
		newSearch.hashTags = $scope.form.hashtags;
		newSearch.url = $scope.form.url;
		newSearch.external = $scope.form.external;
		newSearch.internal = $scope.form.internal;
		newSearch.$save();
		$scope.form = {};
	}

});
