// Create Campaign Controller

app.controller("CreateCampaignController", function($scope, Campaigns) {
	this.form = {};

	var newCampaign = new Campaigns();

	this.campaignSubmit = function(info) {
		newCampaign.client = info.client;
		newCampaign.dateRange = info.dateRange;
		newCampaign.search = info.search;
		newCampaign.status = info.status;
		newCampaign.type = info.type;
		newCampaign.$save();
		this.form = {};
	}
});
