app.controller("CreateClientController", function($scope, $state, Clients) {
	$scope.form = {};

	var newClient = new Clients();

	$scope.submitClient = function() {
		console.log($scope.form);
		newClient.name = $scope.form.name;
		newClient.address = $scope.form.address;
		newClient.addressAddtl = $scope.form.addressAddtl;
		newClient.bizPhone = $scope.form.bizPhone;
		newClient.conName = $scope.form.conName;
		newClient.conPhone = $scope.form.conPhone;
		newClient.conEmail = $scope.form.conEmail;
		newClient.payMethod = $scope.form.payMethod;
		newClient.payCycle = $scope.form.payCycle;
		newClient.payCap = $scope.form.payCap;
		newClient.payCapType = $scope.form.payCapType;
		newClient.$save();
		$scope.form = {};
	}

});
