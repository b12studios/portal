// App.js
	"use strict";
	
	var pageWrapper = "<div class=\"hbox hbox-auto-xs hbox-auto-sm\"><div class=\"col\"><div data-ng-include=\"\'/tplt/page-header.html\'\"></div>		<div class=\"wrapper-md\" ui-view></div></div></div>";

	var $injector = angular.injector(),
			app = angular.module('app', ['ui', 'ui.bootstrap', 'ui.router', 'ngStorage', 'ui.utils', 'ngCookies', 'ui.jq', 'ngResource', 'ngAnimate', "ngSanitize", "ui.load", "d3", "xeditable", "oc.lazyLoad"])
	.constant("JQ_CONFIG", {
		daterangepicker: [
			"../js/lib/moment.min.js",
			"../js/lib/daterangepicker.js",
			"../css/lib/daterangepicker-bs3.css"
		],
		footable: [
			"../js/lib/footable.all.min.js",
			"../css/lib/footable.cose.css"
		],
		xeditable: [
			"../js/lib/xeditable.min.js",
			"../css/lib/xeditable.css"
		]
	})
	.constant("API", {
		url: "http://166.70.63.51:4567/"
	})
	.run([
		'$rootScope', '$state', '$stateParams',
		function ($rootScope, $state, $stateParams, Authorizer, AUTH_EVENTS) {
			$rootScope.$state = $state;
			$rootScope.$stateParams = $stateParams;
			return $rootScope.$on("stateChangeStart", function(event, next) {
				var authenticator, permissions, user;
				permissions = next && next.data ? next.data.permissions : null;
				user = Session.getCurrentUser();
				authenticator = new Authorizer(user);
				if ((permissions !== null) && !authenticator.canAccess(permissions)) {
					event.preventDefault();
					if(!user) {
						return $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
					} else {
						return $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
					}
				}
			});
		}
	]).config([
		'$stateProvider', '$urlRouterProvider', '$httpProvider', '$ocLazyLoadProvider',
		function ($stateProvider, $urlRouterProvider, $httpProvider, $ocLazyLoadProvider) {
			$ocLazyLoadProvider.config({
				debug: !0,
				events: !0,
				modules: [{
					name: "ui.grid",
					file: [
						"/js/lib/uiGrid/ui-grid-stable.min.js",
						"/js/lib/uiGrid/ui-grid-stable.min.css"
					]
				}, {
					name: "ngTable",
					files: [
						"../css/lib/ng-table.min.css",
						"../js/lib/ng-table.min.js"
					]
				}, {
					name: "ui.grid.pagination"
				}]
			});
			$httpProvider.defaults.useXDomain = true;
			$httpProvider.defaults.withCredentials = true;
			delete $httpProvider.defaults.headers.common["X-Requested-With"];
			$httpProvider.defaults.headers.common["Accept"] = "application/json";
			$httpProvider.defaults.headers.common["Content-Type"] = "application/json";
			$httpProvider.defaults.headers.post["Content-Type"] = "application/json";
			$urlRouterProvider.otherwise('/dashboard');
			$stateProvider.state('app', {
					"abstract": !0,
					url: "/",
					templateUrl: 'tplt/home.html'
				})
				// Pages
				.state('app.dashboard', {
					title: "Dashboard",
					description: "Welcome to the investigation dashboard!",
					url: "dashboard",
					templateUrl: 'tplt/dashboard.html'
				})
				.state("app.page", {
					url: "page",
					template: '<div ui-view class="fade-in-right-big"></div>'
				})
				.state("app.page.profile", {
					url: "/profile",
					templateUrl: "../tplt/demo.html",
					resolve: {
						deps: ["uiLoad", function(l) {
							return l.load(["js/controllers/profile.js"])
						}]
					}
				})
				// Manage Pages
				.state('app.manage', {
					url: 'manage',
					template: '<div ui-view></div>'
				})
				.state("app.manage.myInfo", {
					title: "My Information",
					description: "Review and edit your information.",
					url: "/my-info",
					templateUrl: "../tplt/manage/my-info.html",
					controller: "MyInfoController",
					resolve: {
						deps: ["uiLoad", function(l) {
							return l.load(["../js/controllers/manage/my-info.js"])
						}]
					}
				})
				.state("app.manage.billing", {
					title: "My Billing",
					description: "Overview of your billing settings and information.",
					url: "/my-billing",
					templateUrl: "../tplt/manage/my-billing.html"
				})
				.state("app.manage.users", {
					title: "Users",
					description: "Manage and add users to your account.",
					url: "/users",
					templateUrl: "../tplt/manage/users.html",
					resolve: {
						deps: ["$ocLazyLoad", function(o) {
							return o.load([
								"../js/factories/manage-users.js",
								"../js/controllers/manage/users.js",
								"../js/lib/xeditable.min.js",
								"../css/lib/xeditable.css"
							 ])
						}]
					}
				})
				.state("app.manage.groups", {
					title: "Groups",
					description: "Manage and add your groups.",
					url: "/groups",
					templateUrl: "../tplt/manage/groups.html"
				})
				.state("app.manage.quota", {
					title: "Quota Increase Request",
					description: "Make a storage increase request here.",
					url: "/quota-request",
					templateUrl: "../tplt/manage/quota.html"
				})
				// Client Pages
				.state("app.clients", {
					url: "clients",
					template: '<div ui-view></div>'
				})
				.state("app.clients.create", {
					title: "Create Client",
					description: "Create a new record for managed clients",
					url: "/create",
					templateUrl: "../tplt/clients/create.html",
					resolve: {
						deps: ["uiLoad", function(l) {
							return l.load(["../js/factories/clients.js", "../js/controllers/create-client.js"])
						}]
					}
				})
				.state("app.clients.manage", {
					title: "Manage Clients",
					description: "Manage and modify current list of clients.",
					url: "/manage",
					controller: "ManageClientsController",
					templateUrl: "../tplt/clients/manage.html",
					resolve: {
						deps: ["uiLoad", function(l) {
							return l.load('../js/lib/xeditable.min.js','../css/lib/xeditable.css').then(function() {
								return l.load(["../js/factories/clients.js", "../js/controllers/manage-clients.js"])
							})
						}]
					}
				})
				.state("app.clients.view", {
					title: "View Clients",
					description: "List of clients in your account.",
					url: "/view",
					template: pageWrapper
				})
				.state("app.clients.view.all", {
					title: "View Clients",
					description: "List of clients in your account.",
					url: "=all",
					templateUrl: "../tplt/clients/view/all.html",
					resolve: ["uiLoad", function(l) {
						return l.load(['../js/factories/clients.js'])
					}]
				})
				.state("app.clients.view.near", {
					title: "View Clients",
					description: "List of clients in your account.",
					url: "=near",
					templateUrl: "../tplt/clients/view/near.html",
					resolve: ["uiLoad", function(l) {
						return l.load(['../js/factories/clients.js'])
					}]
				})
				.state("app.clients.view.aging", {
					title: "View Clients",
					description: "List of clients in your account.",
					url: "=aging",
					templateUrl: "../tplt/clients/view/aging.html",
					resolve: ["uiLoad", function(l) {
						return l.load(['../js/factories/clients.js'])
					}]
				})
				.state("app.clients.submanage", {
					title: "Sub-Manage of Clients",
					description: " ",
					url: "/sub-manage",
					template: pageWrapper
				})
				.state("app.clients.submanage.users", {
					title: "Sub-Manage of Clients",
					description: "Sub-Manage of Client Users",
					url: "/client-users",
					templateUrl: "../tplt/clients/submanage/client-users.html"
				})
				.state("app.clients.submanage.groups", {
					title: "Sub-Manage of Clients",
					description: "Sub-Manage of Client Groups",
					url: "/client-groups",
					templateUrl: "../tplt/clients/submanage/client-groups.html"
				})
				// Searches
				.state("app.searches", {
					url: "searches",
					template: '<div ui-view></div>'
				})
				.state("app.searches.create", {
					title: "Create New Searches",
					description: "Create new custom searches here.",
					url: "/create",
					templateUrl: "../tplt/searches/create.html",
					controller: "CreateSearchController",
					resolve: {
						deps: ["uiLoad", function(l) {
							return l.load(["../js/factories/searches.js", "../js/controllers/create-search.js", "../js/lib/moment.min.js", "../js/lib/daterangepicker.js", "../css/lib/daterangepicker-bs3.css"]);
						}]
					}
				})
				.state("app.searches.manage", {
					title: "Manage All Searches",
					description: "Manage the current searches here.",
					url: "/manage",
					templateUrl: "../tplt/searches/manage.html"
				})
				.state("app.searches.view", {
					title: "View Searches",
					description: " ",
					url: "/view",
					template: pageWrapper
				})
				.state("app.searches.view.all", {
					title: "View Searches",
					description: "View all searches here.",
					url: "=all",
					templateUrl: "../tplt/searches/view/all.html"
				})
				.state("app.searches.view.global", {
					title: "View Searches",
					description: "View global searches here.",
					url: "=global",
					templateUrl: "../tplt/searches/view/global.html"
				})
				.state("app.searches.view.client", {
					title: "View Searches",
					description: "View client created searches here.",
					url: "=client",
					templateUrl: "../tplt/searches/view/client.html"
				})
				.state("app.searches.view.timed", {
					title: "View Searches",
					description: "View timed searches here.",
					url: "=timed",
					templateUrl: "../tplt/searches/view/timed.html"
				})
				.state("app.searches.request", {
					title: "Request Approval for Searches",
					description: "Requests from clients without the ability to create searches to have a new search created.",
					url: "/request",
					templateUrl: "../tplt/searches/request.html"
				})
				.state("app.searches.orphaned", {
					title: "Ophaned Searches",
					description: "Client created searches running where the campaign has ended. These require action to either remove search or change to global search.",
					url: "/orphaned",
					templateUrl: "../tplt/searches/orphaned.html"
				})
				// Campaign Pages
				.state("app.campaigns", {
					url: "campaigns",
					template: '<div ui-view></div>'
				})
				.state("app.campaigns.create", {
					title: "Create New Campaigns",
					description: "Create new custom campaigns here.",
					url: "/create",
					templateUrl: "../tplt/campaigns/create.html",
					resolve: {
						deps: ["uiLoad", function(l) {
							return l.load(["../js/factories/campaigns.js", "../js/controllers/create-campaign.js", "../js/lib/moment.min.js", "../js/lib/daterangepicker.js", "../css/lib/daterangepicker-bs3.css"]);
						}]
					}
				})
				.state("app.campaigns.manage", {
					title: "Manage Campaigns",
					description: "Manage all current campaigns.",
					ur: "/manage",
					templateUrl: "../tplt/campaigns/manage.html"
				})
				.state("app.campaigns.view", {
					title: "View Searches",
					description: " ",
					url: "/view",
					template: pageWrapper
				})
				.state("app.campaigns.view.all", {
					title: "View Campaigns",
					description: "View all campaigns here.",
					url: "=all",
					templateUrl: "../tplt/campaigns/view/all.html"
				})
				.state("app.campaigns.view.scheduled", {
					title: "View Campaigns",
					description: "View scheduled campaigns here.",
					url: "=scheduled",
					templateUrl: "../tplt/campaigns/view/scheduled.html"
				})
				.state("app.campaigns.view.active", {
					title: "View Campaigns",
					description: "View active campaigns here.",
					url: "=active",
					templateUrl: "../tplt/campaigns/view/active.html"
				})
				.state("app.campaigns.view.completed", {
					title: "View Campaigns",
					description: "View completed campaigns here.",
					url: "=completed",
					templateUrl: "../tplt/campaigns/view/completed.html"
				})
				.state("app.campaigns.view.archived", {
					title: "View Campaigns",
					description: "View archived campaigns here.",
					url: "=archived",
					templateUrl: "../tplt/campaigns/view/archived.html"
				})
				// Investigate
				.state("app.investigate", {
					url: "investigate",
					template: '<div ui-view></div>'
				})
				.state("app.investigate.triage", {
					title: "Triage",
					description: " ",
					url: "/triage",
					template: pageWrapper
				})
				.state("app.investigate.triage.queue", {
					title: "Triage Queue",
					description: "Shows queue information and allows selective processing into Start Work.",
					url: "/queue",
					templateUrl: "../tplt/investigate/triage/queue.html"
				})
				.state("app.investigate.triage.work", {
					title: "Triage Start Work",
					description: "The triage workflow page.",
					url: "/start-work",
					templateUrl: "../tplt/investigate/triage/work.html"
				})
				.state("app.investigate.investigation", {
					title: "Investigation",
					description: " ",
					url: "/investigation",
					template: pageWrapper
				})
				.state("app.investigate.investigation.queue", {
					title: "Investigation Queue",
					description: "Shows queue information and allows selective processing into Start Work.",
					url: "/queue",
					templateUrl: "../tplt/investigate/investigation/queue.html"
				})
				.state("app.investigate.investigation.work", {
					title: "Investigation Start Work",
					description: "The investigation workflow page.",
					url: "/start-work",
					templateUrl: "../tplt/investigate/investigation/work.html"
				})
				.state("app.investigate.actionable", {
					title: "Actionable",
					description: " ",
					url: "/actionable",
					template: pageWrapper
				})
				.state("app.investigate.actionable.queue", {
					title: "Actionable Queue",
					description: "Shows queue information and allows selective processing into Start Work.",
					url: "/queue",
					templateUrl: "../tplt/investigate/actionable/queue.html"
				})
				.state("app.investigate.actionable.work", {
					title: "Actionable Start Work",
					description: "The actionable workflow page.",
					url: "/start-work",
					templateUrl: "../tplt/investigate/actionable/work.html"
				})
				// Reports
				.state("app.reports", {
					url: "reports",
					template: '<div ui-view></div>'
				})
				.state("app.reports.workforce", {
					title: "Workforce Reports",
					description: "Global Performance, Departmental Performance, Employee Report, Employee Compare, Top 10, WorkGap",
					url: "/workforce",
					templateUrl: "../tplt/reports/workforce.html"
				})
				.state("app.reports.dataset", {
					title: "Dataset Reports",
					description: "MD5 Report, Coupon Report, Barcode Report, VS Report, Custom Report",
					url: "/dataset",
					templateUrl: "../tplt/reports/dataset.html"
				})
				.state("app.reports.clients", {
					title: "Clients Reports",
					description: "New clients day over day, campaigns (total / per client day over day), Audit Log",
					url: "/clients",
					templateUrl: "../tplt/reports/clients.html"
				})
				.state("app.reports.statisics", {
					title: "Statistics",
					description: "Queues hour by hour ( etc ), Backprocess hour by hour, Objects Stored houor by hour ( inc count/size ), Image Processing hour by hour, Datasource Stats hour by hour",
					url: "/statisics",
					templateUrl: "../tplt/reports/statisics.html"
				})
				// FBI
				.state("app.fbi", {
					url: "fbi",
					template: '<div ui-view></div>'
				})
				.state("app.fbi.md5", {
					title: "MD5 Matches",
					description: " ",
					url: "/md5",
					templateUrl: "../tplt/fbi/md5.html"
				})
				.state("app.fbi.barcode", {
					title: "Barcode Matches",
					description: " ",
					url: "/barcode",
					templateUrl: "../tplt/fbi/barcode.html"
				})
				// Access Pages
				.state("access", {
					url: "access",
				})
				.state("access.signin", {
					url: "/login",
					templateUrl: "../tplt/login.html",
					resolve: {
						deps: ["uiLoad", function (l) {
							return l.load(["/js/controllers/login.js"])
						}]
					}
				})
				.state('user', {
					url: '/user'
				})
				.state('app.account', {
					url: '/account'
				})
		}
	]);
	
	app.controller("AppController", ["$scope", "$localStorage", "$window", "$http", function (a, b, c, d) {
		var f = !!navigator.userAgent.match(/MSIE/i);
		d.get("/data.json").success(function(r) { a.data = r; });
		f && angular.element(d.document.body).addClass("ie"),
			a.app = {
				name: "Investigate",
				version: "0.1.27",
				settings: {
					headerFixed: !0,
					asideFixed: !1,
					asideFolded: !1,
					asideDock: !1,
					container: !1
				}
			},
			a.user = {
				fistName: "That",
				lastName: "Guy",
				name: "That Guy",
				username: "someDude",
				permissions: 4 // 4 is admin, 3 is management, 2 is investigator, 1 is classification agent, 0 is triage agent
			},
			angular.isDefined(c.settings) ? a.app.settings = c.settings : c.settings = a.app.settings,
			a.$watch("app.settings", function () {
				a.app.settings.asideDock && a.app.settings.asideFixed && (a.app.settings.headerFixed = !0), c.settings = a.app.settings
			}, !0)
	}]), app.controller("DatepickerController", ["$scope", function (a) {
		a.today = function () {
			a.dt = new Date
		}, a.today(), a.clear = function () {
			a.dt = null
		}, a.disabled = function (a, b) {
			return "day" === b && (0 === a.getDay() || 6 === a.getDay())
		}, a.toggleMin = function () {
			a.minDate = a.minDate ? null : new Date
		}, a.toggleMin(), a.open = function (b) {
			b.preventDefault(), b.stopPropagation(), a.opened = !0
		}, a.dateOptions = {
			formatYear: "yy",
			startingDay: 1,
			"class": "datepicker"
		}, a.initDate = new Date("2016-15-20"), a.formats = ["dd-MMMM-yyyy", "yyyy/MM/dd", "dd.MM.yyyy", "shortDate"], a.format = a.formats[0]
	}]), app.controller("TimepickerController", ["$scope", function (a) {
		a.mytime = new Date, a.hstep = 1, a.mstep = 15, a.options = {
			hstep: [1, 2, 3],
			mstep: [1, 5, 10, 15, 25, 30]
		}, a.ismeridian = !0, a.toggleMode = function () {
			a.ismeridian = !a.ismeridian
		}, a.update = function () {
			var b = new Date;
			b.setHours(14), b.setMinutes(0), a.mytime = b
		}, a.changed = function () {}, a.clear = function () {
			a.mytime = null
		}
	}]), app.controller("NewCampaignController", ["$http", function(a) {
		this.form = {};
		this.campaignSubmit = function(info) {
			console.log(info);
			info = this.form;
			this.form = {};
		};
	}]), app.controller("CurrentCampaignsController", ["$http", function() {
		
	}]), app.controller("NewClientController", ["$http", function() {
		this.form = {};
		this.clientSubmit = function(info) {
			console.log(this.form);
			this.form = {};
		}
	}]), app.controller("CurrentClientsController", ["$http", function() {
		
	}]), app.controller("NewSearchController", ["$scope", "$http", function(a, b) {
		this.form = {};
		this.searchSubmit = function(info) {
			console.log(a.form);
			console.log(info);
			this.form = {};
		};
		b.get("data/boards.json").success(function(r) { a.boards = r.boards; });
		b.get("http://www.reddit.com/reddits.json?limit=999").success(function(r) { a.reddits = r.data.children; });
	}]), app.controller("CurrentSearchesController", ["$http", function() {
		
	}]), angular.module("app").directive("setNgAnimate", ["$animate", function (a) {
		return {
			link: function (b, c, d) {
				b.$watch(function () {
					return b.$eval(d.setNgAnimate, b)
				}, function (b) {
					a.enabled(!!b, c)
				})
			}
		}
	}]), angular.module("app").directive("uiButterbar", ["$rootScope", "$anchorScroll", function (a, b) {
		return {
			restrict: "AC",
			template: '<span class="bar"></span>',
			link: function (a, c) {
				c.addClass("butterbar hide"), a.$on("$stateChangeStart", function () {
					b(), c.removeClass("hide").addClass("active")
				}), a.$on("$stateChangeSuccess", function (a) {
					a.targetScope.$watch("$viewContentLoaded", function () {
						c.addClass("hide").removeClass("active")
					})
				})
			}
		}
	}]), angular.module("ui.jq", ["ui.load"]).value("uiJqConfig", {}).directive("uiJq", ["uiJqConfig", "JQ_CONFIG", "uiLoad", "$timeout", function (a, b, c, d) {
		return {
			restrict: "A",
			compile: function (e, f) {
				if (!angular.isFunction(e[f.uiJq]) && !b[f.uiJq]) throw new Error('ui-jq: The "' + f.uiJq + '" function does not exist');
				var g = a && a[f.uiJq];
				return function (a, e, f) {
					function h() {
						var b = [];
						return f.uiOptions ? (b = a.$eval("[" + f.uiOptions + "]"), angular.isObject(g) && angular.isObject(b[0]) && (b[0] = angular.extend({}, g, b[0]))) : g && (b = [g]), b
					}

					function i() {
						d(function () {
							e[f.uiJq].apply(e, h())
						}, 0, !1)
					}

					function j() {
						f.uiRefresh && a.$watch(f.uiRefresh, function () {
							i()
						})
					}
					f.ngModel && e.is("select,input,textarea") && e.bind("change", function () {
						e.trigger("input")
					}), b[f.uiJq] ? c.load(b[f.uiJq]).then(function () {
						i(), j()
					})["catch"](function () {}) : (i(), j())
				}
			}
		}
	}]), angular.module("app").directive("uiModule", ["MODULE_CONFIG", "uiLoad", "$compile", function (a, b, c) {
		return {
			restrict: "A",
			compile: function (d) {
				var e = d.contents().clone();
				return function (d, f, g) {
					f.contents().remove(), b.load(a[g.uiModule]).then(function () {
						c(e)(d, function (a) {
							f.append(a)
						})
					})
				}
			}
		}
	}]), angular.module("app").directive("uiNav", ["$timeout", function () {
		return {
			restrict: "AC",
			link: function (a, b) {
				var c, d = $(window),
					e = 768,
					f = $(".app-aside"),
					g = ".dropdown-backdrop";
				b.on("click", "a", function (a) {
					c && c.trigger("mouseleave.nav");
					var b = $(this);
					b.parent().siblings(".active").toggleClass("active"), b.next().is("ul") && b.parent().toggleClass("active") && a.preventDefault(), b.next().is("ul") || d.width() < e && $(".app-aside").removeClass("show off-screen")
				}), b.on("mouseenter", "a", function (a) {
					if (c && c.trigger("mouseleave.nav"), $("> .nav", f).remove(), !(!$(".app-aside-fixed.app-aside-folded").length || d.width() < e || $(".app-aside-dock").length)) {
						var b, h = $(a.target),
							i = $(window).height(),
							j = 50,
							k = 150;
						!h.is("a") && (h = h.closest("a")), h.next().is("ul") && (c = h.next(), h.parent().addClass("active"), b = h.parent().position().top + j, c.css("top", b), b + c.height() > i && c.css("bottom", 0), b + k > i && c.css("bottom", i - b - j).css("top", "auto"), c.appendTo(f), c.on("mouseleave.nav", function () {
							$(g).remove(), c.appendTo(h.parent()), c.off("mouseleave.nav").css("top", "auto").css("bottom", "auto"), h.parent().removeClass("active")
						}), $(".smart").length && $('<div class="dropdown-backdrop"/>').insertAfter(".app-aside").on("click", function (a) {
							a && a.trigger("mouseleave.nav")
						}))
					}
				}), f.on("mouseleave", function () {
					c && c.trigger("mouseleave.nav"), $("> .nav", f).remove()
				})
			}
		}
	}]), angular.module("app").directive("uiScrollTo", ["$location", "$anchorScroll", function (a, b) {
		return {
			restrict: "AC",
			link: function (c, d, e) {
				d.on("click", function () {
					a.hash(e.uiScrollTo), b()
				})
			}
		}
	}]), angular.module("app").directive("uiShift", ["$timeout", function (a) {
		return {
			restrict: "A",
			link: function (b, c, d) {
				function e() {
					a(function () {
						var a = d.uiShift,
							b = d.target;
						h.hasClass("in") || h[a](b).addClass("in")
					})
				}

				function f() {
					g && g.prepend(c), !g && h.insertAfter(j), h.removeClass("in")
				}
				var g, h = $(c),
					i = $(window),
					j = h.prev(),
					k = i.width();
				!j.length && (g = h.parent()), 768 > k && e() || f(), i.resize(function () {
					k !== i.width() && a(function () {
						i.width() < 768 && e() || f(), k = i.width()
					})
				})
			}
		}
	}]), angular.module("app").directive("uiToggleClass", ["$timeout", "$document", function () {
		return {
			restrict: "AC",
			link: function (a, b, c) {
				b.on("click", function (a) {
					function d(a, b) {
						for (var c = new RegExp("\\s" + a.replace(/\*/g, "[A-Za-z0-9-_]+").split(" ").join("\\s|\\s") + "\\s", "g"), d = " " + $(b)[0].className + " "; c.test(d);) d = d.replace(c, " ");
						$(b)[0].className = $.trim(d)
					}
					a.preventDefault();
					var e = c.uiToggleClass.split(","),
						f = c.target && c.target.split(",") || Array(b),
						g = 0;
					angular.forEach(e, function (a) {
						var b = f[f.length && g]; - 1 !== a.indexOf("*") && d(a, b), $(b).toggleClass(a), g++
					}), $(b).toggleClass("active")
				})
			}
		}
	}]).service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
	}), angular.module("ui.load", []).service("uiLoad", ["$document", "$q", "$timeout", function (a, b, c) {
		var d = [],
			e = !1,
			f = b.defer();
		this.load = function (a) {
			a = angular.isArray(a) ? a : a.split(/\s+/);
			var b = this;
			return e || (e = f.promise), angular.forEach(a, function (a) {
				e = e.then(function () {
					return a.indexOf(".css") >= 0 ? b.loadCSS(a) : b.loadScript(a)
				})
			}), f.resolve(), e
		}, this.loadScript = function (e) {
			if (d[e]) return d[e].promise;
			var f = b.defer(),
				g = a[0].createElement("script");
			return g.src = e, g.onload = function (a) {
				c(function () {
					f.resolve(a)
				})
			}, g.onerror = function (a) {
				c(function () {
					f.reject(a)
				})
			}, a[0].body.appendChild(g), d[e] = f, f.promise
		}, this.loadCSS = function (e) {
			if (d[e]) return d[e].promise;
			var f = b.defer(),
				g = a[0].createElement("link");
			return g.rel = "stylesheet", g.type = "text/css", g.href = e, g.onload = function (a) {
				c(function () {
					f.resolve(a)
				})
			}, g.onerror = function (a) {
				c(function () {
					f.reject(a)
				})
			}, a[0].head.appendChild(g), d[e] = f, f.promise
		}
	}]), angular.module("app").filter("fromNow", function () {
		return function (a) {
			return moment(a).fromNow()
		}
	}), angular.module("d3", []).factory('d3Service', ["$document", "$q", "$rootScope", function($document, $q, $rootScope) {
		var d = $q.defer();
		function onScriptLoad() {
			// Load client in browser
			$rootScope.$apply(function() { d.resolve(window.d3); });
		}
		
		// Create script tag with d3
		var scriptTag = $document[0].createElement('script');
		scriptTag.type = "text/javascript";
		scriptTag.async = true;
		scriptTag.src = 'https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js';
		scriptTag.onreadystatechange = function () {
			if (this.readyState == "complete") onScriptLoad();
		}
		scriptTag.onload = onScriptLoad;
		
		var s = $document[0].getElementsByTagName('body')[0];
		s.appendChild(scriptTag);
	
		return {
			d3: function() { return d.promise; }
		};
	}]), angular.module("app").directive("d3Object", ["$window", "$timeout", "d3Service", function($window, $timeout, d3Service) {
    return {
      restrict: 'A',
      scope: {
        data: '=',
        label: '@',
        onClick: '&'
      },
      link: function(scope, ele, attrs) {
				var m = scope.data.length + 1;
				d3Service.d3().then(function(d3) {
 
          var renderTimeout,
          		margin = {
								top: parseInt(attrs.marginTop) || parseInt(attrs.margin) || 5,
								left: parseInt(attrs.marginLeft) || parseInt(attrs.margin) || 5,
								right: parseInt(attrs.marginRight) || parseInt(attrs.margin) || 5,
								bottom: parseInt(attrs.marginBottom) || parseInt(attrs.margin) || 5
							},
              barHeight = parseInt(attrs.barHeight) || 20,
              barPadding = attrs.barPadding || 5,
							barWidth = parseInt(attrs.barWidth) || scope.data.length / 3,
							chart = attrs.d3Chart || "bar",
							svgHeight = attrs.height || "100%",
							svgWidth = attrs.width || "100%";
					
					var svg = d3.select(ele[0])
          		.append('svg')
          		.style('width', svgWidth)
							.style('height', svgHeight);

          $window.onresize = function() {
            scope.$apply();
          };
 
          scope.$watch(function() {
            return angular.element($window)[0].innerWidth;
          }, function() {
            scope.render(scope.data);
          });
 
          scope.$watch('data', function(newData) {
            scope.render(newData);
          }, true);
 
          scope.render = function(data) {
						svg.selectAll('*').remove();
 
            if (!data) return;
            if (renderTimeout) clearTimeout(renderTimeout);
						
						
						var	width = d3.select(ele[0])[0][0].offsetWidth + margin.left + margin.right,
								height = d3.select(ele[0])[0][0].offsetHeight + margin.top + margin.bottom;
						
						renderTimeout = $timeout(function() {
							if (chart === "bar") {
								var n = 1, // Number of layers
										stack = d3.layout.stack(),
										layers = stack(d3.range(n).map(function() { return data; })),
										yStackMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.value0 + d.value; }); }),
										yGroupMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.value; }); });

								var xScale = d3.scale.ordinal().domain(d3.range(m)).rangeRoundBands([-20, width], .08),
										yScale = d3.scale.linear().domain([0, yGroupMax]).range([height, 0]),
										color = d3.scale.linear().domain([0, n - 1]).range(["#aad", "#446"]),
										xAxis = d3.svg.axis().scale(xScale).tickSize(0).tickPadding(6).orient("bottom"),
										layer = svg.selectAll(".layer").data(layers).enter().append("g").attr("class", "layer"),
										rect = layer.selectAll("rect")
																.data(function(d) { return d; })
																.enter()
																.append("rect")
																.style("fill", function(d, i) { return color(d.index); })
																.attr("x", function(d) { return xScale(d.index); })
																.attr("y", height)
																.attr("width", xScale.rangeBand())
																.attr("height", 0);

								svg.append("g")
									 .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

								rect.transition()
										.delay(function(d, i) { return i * 10; })
										.attr("x", function(d, i, j) { return xScale(d.index) + xScale.rangeBand() / n * j; })
										.attr("width", barWidth)
										.style("margin-right", barPadding + "px")
										.transition()
										.attr("y", function(d) { return yScale(d.value); })
										.attr("height", function(d) { return height - yScale(d.value); });

								svg.append("g")
										.attr("class", "x axis")
										.attr("transform", "translate(0," + height + ")")
										.attr(xAxis);
							} 
							
							if (chart === "pie") {
								var pie = d3.layout.pie(),
										color = d3.scale.category10();
								
								var arc = d3.svg.arc()
									.innerRadius(width / 4)
									.outerRadius(width / 2);
								
								var arcs = svg.selectAll("g.pie")
									.data(pie(d3.range(1).map(function() { return data; })))
									.enter()
									.append("g")
									.attr("class", "pie")
									.attr("transform", "traslate(" + width / 2 + "," + width / 2 + ")");							
								
								arcs.append("path")
									.attr("fill", function(d, i) {
										return color(i);
									})
									.attr("d", arc);
								
								arcs.append("text")
									.attr("transform", function(d) {
										return "translate(" + arc.centroid(d.value) + ")";
									})
									.attr("text-anchor", "middle")
									.text(function(d) {
										return d.text;
									});
								
							}
							
            }, 200);
          };
        });
      }};
	}]);

	angular.module("app").config(["$controllerProvider", "$compileProvider", "$filterProvider", "$provide", function (a, b, c, d) {
		app.controller = a.register, app.directive = b.directive, app.filter = c.register, app.factory = d.factory, app.service = d.service, app.constant = d.constant, app.value = d.value
	}]);
