<ul class="nav navbar-nav hidden-sm">
		<li class="dropdown pos-stc" dropdown>
			<a href class="dropdown-toggle" dropdown-toggle>
				<span>Operations</span>
				<span class="caret"></span>
			</a>
			<div class="dropdown-menu wrapper w-full bg-white">
				<div class="row">
					<div class="col-sm-4">
						<div class="mgn-lft-xs mgn-top-xs mgn-btm-xs font-bold">Pages <span class="badge badge-sm bg-success">10</span>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<ul class="list-unstyled l-h-2x">
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Profile</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Post</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Search</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Invoice</a>
									</li>
								</ul>
							</div>
							<div class="col-xs-6">
								<ul class="list-unstyled l-h-2x">
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Price</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Lock screen</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Sign in</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Sign up</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4 b-l b-light">
						<div class="mgn-lft-xs mgn-top-xs mgn-btm-xs font-bold">UI Kits <span class="label label-sm bg-primary">12</span>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<ul class="list-unstyled l-h-2x">
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Buttons</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Icons <span class="badge badge-sm bg-warning">1000+</span></a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Grid</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Widgets</a>
									</li>
								</ul>
							</div>
							<div class="col-xs-6">
								<ul class="list-unstyled l-h-2x">
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Bootstap</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Sortable</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Portlet</a>
									</li>
									<li>
										<a href><i class="fa fa-fw fa-angle-right text-muted mgn-rgt-xs"></i>Timeline</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4 b-l b-light">
						<div class="mgn-lft-xs mgn-top-xs mgn-btm-sm font-bold">Analysis</div>
						<div class="text-center">
							<div class="inline">
								<div d3-object d3-chart="bar" margin="2" bar-width="1" data="data" style="width: 300px; height: 155px">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="dropdown" dropdown>
			<a href class="dropdown-toggle" dropdown-toggle>
				<i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
				<span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" translate="header.navbar.new.PROJECT">Projects</a>
				</li>
				<li>
					<a href>
						<span class="badge bg-info pull-right">5</span>
						<span translate="header.navbar.new.TASK">Task</span>
					</a>
				</li>
				<li><a href translate="header.navbar.new.USER">User</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href>
						<span class="badge bg-danger pull-right">4</span>
						<span translate="header.navbar.new.EMAIL">Email</span>
					</a>
				</li>
			</ul>
		</li>
	</ul>
	<!-- / link and dropdown -->

	<!-- navbar right -->
	<ul class="nav navbar-nav navbar-right">
		<!-- search form -->
		<form class="navbar-form navbar-form-sm navbar-left" target=".navbar-collapse" role="search">
			<div class="form-group">
				<div class="input-group">
					<input type="text" ng-model="selected" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search...">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</form>
		<!-- / search form -->
		<li class="dropdown" dropdown>
			<a href class="dropdown-toggle" dropdown-toggle>
				<i class="fa fa-bell-o"></i>
				<span class="visible-xs-inline">Notifications</span>
				<span class="badge badge-sm up btn-danger pull-right-xs">2</span>
			</a>
			<!-- dropdown -->
			<div class="dropdown-menu w-xl">
				<div class="panel bg-white">
					<div class="panel-heading b-light bg-light">
						<strong>You have <span>2</span> notifications</strong>
					</div>
					<div class="list-group">
						<a href class="media list-group-item">
							<span class="media-body block mgn-btm-none">
								Use awesome animate.css<br>
								<small class="text-muted">10 minutes ago</small>
							</span>
						</a>
						<a href class="media list-group-item">
							<span class="media-body block mgn-btm-none">
								1.0 initial released<br>
								<small class="text-muted">1 hour ago</small>
							</span>
						</a>
					</div>
					<div class="panel-footer text-sm">
						<a href class="pull-right"><i class="fa fa-cog"></i></a>
						<a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
					</div>
				</div>
			</div>
			<!-- / dropdown -->
		</li>
		<li class="dropdown" dropdown>
			<a href class="dropdown-toggle clear" dropdown-toggle>
				<span class="hidden-sm hidden-md">{{user.name}}</span> <b class="caret"></b>
			</a>
			<!-- dropdown -->
			<ul class="dropdown-menu animated fadeInRight w">
				<li class="wrapper b-b mgn-btm-sm bg-light mgn-top-neg-xs">
					<div>
						<p>300mb of 500mb used</p>
					</div>
					<progressbar value="60" class="progress-xs mgn-btm-none bg-white"></progressbar>
				</li>
				<li>
					<a href>
						<span class="badge bg-danger pull-right">30%</span>
						<span>Settings</span>
					</a>
				</li>
				<li>
					<a ui-sref="app.page.profile">Profile</a>
				</li>
				<li>
					<a ui-sref="app.docs">
						<span class="label bg-info pull-right">new</span> Help
					</a>
				</li>
				<li class="divider"></li>
				<li>
					<a ui-sref="access.signin">Logout</a>
				</li>
			</ul>
			<!-- / dropdown -->
		</li>
	</ul>
	<!-- / navbar right -->
